---
layout: handbook-page-toc
title: "Employee Enablement Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Business Technology Employee Enablement Team/IT Handbook Page

The Business Technology Employee Enablement Team consists of IT Ops System Administrators and IT Helpdesk Analysts who are managed by our IT Manager. 

* IT Operations
    * **[Open an issue](https://gitlab.com/gitlab-com/business-ops/itops/issue-tracker/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)**
    * Our [IT Operations Handbook Page](/handbook/business-ops/it-ops-team/) gives an overview of our IT Operations team, how to contact our team, and discusses our Access Request, Laptop, and Okta policies.
* IT Helpdesk
    * **[Open an issue](https://gitlab.com/gitlab-com/business-ops/IT-help/hd-issue-tracker/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)**
    * Our [IT Help Handbook Page](/handbook/business-ops/it-help/) gives an overview of our IT Help team, how to contact our team, and discusses some Self-Help and Troubleshooting tips.

## Access Requests

*  [Open an Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
*  For information about the access request policies and security guidelines, please refer to the Security Team's [access request handbook page section](/handbook/engineering/security/#access-management-process).
*  For links to role based access request templates, system access templates, and other general instructions and FAQs, please refer to the [Access Requests page](/handbook/business-ops/it-ops-team/access-requests/).
*  For a diagram of how baseline entitlements work and what systems you must request to access other systems, [please refer to the diagram page.](/handbook/business-ops/it-ops-team/access-requests/baseline_flow/)

### Baseline & Role-Based Entitlements

For information about baseline entitlements and role-based access, please refer to the [baseline entitlements handbook page](/handbook/business-ops/it-ops-team/access-requests/#baseline-role-based-entitlements-access-runbooks--issue-templates).

For information on how to create a Role-Based Entitlement, please refer to the [instructions on how to create role-based entitlements](/handbook/business-ops/it-ops-team/access-requests/#how-do-i-choose-which-template-to-use ).

## Automated Group Membership Reports for Managers

If you would like to check whether or not a team-member is a member of a Slack or a G-Suite group, you can view the following automated group membership reports:

[G-Suite Group Membership Reports](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members)

[Slack Group Membership Reports](https://gitlab.com/gitlab-com/security-tools/report-slack-group-members)

## Okta

In an effort to secure access to systems, GitLab is utilizing Okta.
The key goals are:

* We can use Okta to enable Zero-Trust based authentication controls upon our assets, so that we can allow authorized connections to key assets with a greater degree of certainty.
* We can better manage the login process to the 80+ and growing cloud applications that we use within our tech stack.
* We can better manage the Provisioning and De-provisioning process for our users to access these application, by use of automation and integration into our HRIS system.
* We can make Trust and Risk based decisions on authentication requirements to key assets, and adapt these to ensure a consistent user experience.

To read more about Okta, please visit the [**Okta**](/handbook/business-ops/okta/) page of the handbook.
