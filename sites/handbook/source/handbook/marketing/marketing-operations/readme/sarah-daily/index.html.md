---
layout: markdown_page
title: "Sarah Daily's README"
---

# Sarah Daily's — README.md

I created this personal README document to help you get to know me and how I work. This document is inspired by Tim Hey's [README](https://about.gitlab.com/handbook/product/readme/tim-hey.html) and the [Product team README's](https://about.gitlab.com/handbook/product/readme/#product-readmes).

* [GitLab Profile](https://gitlab.com/sdaily)
* [Team Page](https://about.gitlab.com/company/team/#sdaily)
* [LinkedIn](https://www.linkedin.com/in/sarahdaily/)

## About me

* My name is Sarah Daily (last name jokes are welcome and I've also had some people just call me `Daily`). I am a Marketing Operations Manager on the [Marketing Operations team](https://about.gitlab.com/handbook/marketing/marketing-operations/) at GitLab and I focus on content.
* I currently live in [Pagosa Springs, CO](https://www.google.com/maps/place/Pagosa+Springs,+CO+81147/@37.2673803,-107.0497475,14z/data=!3m1!4b1!4m5!3m4!1s0x873ddb6e9b07b449:0x7b8616cc41f8157f!8m2!3d37.26945!4d-107.0097617) with my partner Becca and our two dogs [MaddieJo and Chewie](https://about.gitlab.com/company/team-pets/#132-maddiejo-and-chewie).
* I was born and raised in northeastern Oklahoma.
* I am an [Advocate](https://www.16personalities.com/infj-personality) ([INFJ-A / INFJ-T](https://www.16personalities.com/articles/assertive-advocate-infj-a-vs-turbulent-advocate-infj-t)) personality type.
* I studied media and convergence journalism at Oral Roberts University in Tulsa, Oklahoma. Convergence journlism is just a fancy term to cover media in all channels (online and offline).
* I enjoy gaming in all its form (console, PC, board). I'm active on the `#gaming` Slack channel and I'm a huge fan of retro games and video game music. I have multiple playlists on [Spotify](https://open.spotify.com/user/sarahddaily?si=fKwu3BAgSce5j_I7e14vww) solely dedicated to my favorite video game music.

## How I work

* My timezone is [MST](https://time.is/MT)
* My typical working hours are 7 AM to 4 PM with a 1/2-hour to hour lunch break around 11 AM.
* I follow the [Marketing Operations work cadence](https://about.gitlab.com/handbook/marketing/marketing-operations/#operations-work-cadence) of 2-week sprints tracked as [`Milestones`](https://docs.gitlab.com/ee/user/project/milestones/index.html#overview).

## My typical week

* In Marketing Operations, we have `No meeting Mondays`. I reserve Monday for deep work related to [current milestone issues](https://gitlab.com/groups/gitlab-com/-/milestones?utf8=%E2%9C%93&search_title=mktg&state=&sort=).
* I have most of my meetings Tuesday - Friday.
* I clean up my to-dos and MRs every Friday.
* I use Clockwise to block focus time for me during the week.

## My typical day

* I get up at 6:30 AM on most days (if my dogs don’t wake me up first).
* I start working at 7 AM and take my first break around 11 AM. 
* I typically address emails, to-dos, and Slack DMs first.
* I prefer to have meetings in the morning but will meet anytime during my normal working hours. 

## What’s keeping me busy

* [My open issues](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username[]=sdaily)

## Tools I admin

* [PathFactory](https://about.gitlab.com/handbook/marketing/marketing-operations/pathfactory)
* Sigstr
* Hotjar
* Disqus
* Cookiebot
* [Litmus](https://about.gitlab.com/handbook/marketing/marketing-operations/litmus)
* Sprout Social
* [YouTube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/)
* Vimeo
* [RushTranslate](https://about.gitlab.com/handbook/marketing/marketing-operations/rushtranslate/)
* Rev.com
* Swiftype

## Issue templates I'm mentioned in

* [Gated-Content-Request-Analysts-MPM.md](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/blob/master/.gitlab/issue_templates/Gated-Content-Request-Analysts-MPM.md)
* [Gated-Content-Request-MPM.md](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/blob/master/.gitlab/issue_templates/Gated-Content-Request-MPM.md)
* [pathfactory_request.md](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/blob/master/.gitlab/issue_templates/pathfactory_request.md)
* [mktgops_team_standup.md](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/blob/master/.gitlab/issue_templates/mktgops_team_standup.md)
* [pathfactory_request.md (MOps)](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/blob/master/.gitlab/issue_templates/pathfactory_request.md)
* [pf-listening-campaign.md](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/blob/master/.gitlab/issue_templates/pf-listening-campaign.md)

## How to reach me

* **Slack**: Direct message or mention is the best way to reach me during the day (my handle is `@sdaily`). I will always have Slack open during my typical working hours. If you have an urgent message or need me to repond quickly the best approach is a direct message with a link to the issue you need me to contribute to. I like to focus deeply on the task at hand, so please expect my responses on email and Slack to be [asynchronous](https://about.gitlab.com/handbook/communication/). Your messages are important to me and I will respond as soon as I can.
* **Coffee chat**: You can schedule a 30 min coffee chat with me via **[Calendly](https://calendly.com/sdaily/30min)**.
