---
layout: handbook-page-toc
title: "Create:Source Code BE Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Create:Source Code BE team is responsible for all backend aspects of the product categories that fall under the [Source Code group][group] of the [Create stage][stage] of the [DevOps lifecycle][lifecycle].

[group]: /handbook/product/categories/#source-code-group
[stage]: /handbook/product/categories/#create-stage
[lifecycle]: /handbook/product/categories/#devops-stages

## Team Members

The following people are permanent members of the Create:Source Code BE Team:

<%= direct_team(manager_role: 'Backend Engineering Manager, Create:Source Code') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] (Create(?!:)|Create:Source Code)/, direct_manager_role: 'Backend Engineering Manager, Create:Source Code') %>

## Sisense & KPIs

<%= partial("handbook/engineering/development/dev/create-source-code-be/metrics.erb") %>

## Hiring

This chart shows the progress we're making on hiring. Check out our
[jobs page](/jobs/) for current openings.

<%= hiring_chart(department: 'Create:Source Code BE Team') %>

## Work

In general, we use the standard GitLab [engineering workflow]. To get in touch
with the Create:Source Code BE team, it's best to create an issue in the relevant project
(typically [GitLab]) and add the `~"group::source code"` and `~backend` labels, along with any other
appropriate labels. Then, feel free to ping the relevant Product Manager and/or
Engineering Manager as listed above.

For more urgent items, feel free to use [#g_create_source_code] on Slack.

[engineering workflow]: /handbook/engineering/workflow/
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[#g_create_source_code]: https://gitlab.slack.com/archives/g_create_source_code

### Working with product

Weekly calls between the product manager and engineering managers (frontend and backend) are listed in the "Source Code Group" calendar. Everyone is welcome to join and these calls are used to discuss any roadblocks, concerns, status updates, deliverables, or other thoughts that impact the group. Every 2 weeks (in the middle of a release), a mid-milestone check-in occurs, to report on the current status of `~"Deliverable"`s. Monthly calls occurs under the same calendar where the entire group is encouraged to join, in order to highlight accomplishments/improvements, discuss future iterations, review retrospective concerns and action items, and any other general items that impact the group.

### Collaborating with other counter parts

You are encouraged to work as closely as needed with stable counterparts outside of the PM. We specifically include quality engineering and application security counterparts prior to a release kickoff and as-needed during code reviews or issue concerns.

Quality engineering is included in our workflow via the [Quad Planning Process](https://gitlab.com/gitlab-com/www-gitlab-com/issues/6318).

Application Security will be involved in our workflow at the same time that [kickoff emails](#kickoff-emails) are sent to the team, so that they are able to review the upcoming milestone work, and notate any concerns or potential risks that we should be aware of.


### Capacity planning

<%= partial("handbook/engineering/development/dev/create/capacity_planning.erb") %>

#### Availability

Approximately 5-10 business days before the start of a new release, the EM will begin determining how "available" the team will be. Some of the things that will be taken into account when determining availability are:

* Upcoming training
* Upcoming time off / holidays
* Upcoming on-call slots
* Potential time spent on another teams deliverables

Availability is a percentage calculated by _(work days available / work days in release) * 100_.

All individual contributors start with a "weight budget" of 10, meaning they are capable (based on historical data) of completing a maximum number of issues worth 10 weight points total (IE: 2 issues which are weighted at 5 and 5, or 10 issues weighted at 1 each, etc.) Then, based on their availability percentage, weight budgets are reduced individually. For example, if you are 80% available, your weight budget becomes 8.

Product will prioritize issues based on the teams total weight budget. Our [planning rotation](#planning-rotation) will help assign weights to issues that product intends on prioritizing, to help gauge the amount of work prioritized versus the amount we can handle prior to a kickoff.

#### Kickoff emails

Once availability has been determined, weights have been assigned, and the PM/EM finalize a list of prioritized issues for the upcoming release, kickoff emails will be sent. The intent of this email is to notify you of the work we intend to assign for the upcoming release. This email will be sent before the release begins. The kickoff email will include:

* Your availability, weight budget, and how it was calculated
* A list of the issues you will most probably be assigned as an individual
* A reasoning behind why you have been assigned more than your weight budget, if applicable
* A list of the issues the team is working on that are deemed "note-worthy," in case you'd like to offer help on those issues as time allows

Emails get sent to each individual contributor on the team, as well as the Application Security counterpart, in order to give a heads-up about the upcoming issues in the milestone and what the assignments will be.

#### Planning rotation

_For context, this process was taken and modified from [the Plan team](/handbook/engineering/development/dev/plan/#capacity-planning)._

To assign weights to issues in a future milestone, we ask one team member to
take the lead each month. They can still ask questions - of the
rest of the team, of the stable counterparts, or anyone else - but they are
primarily responsible. Issue weights will be confirmed once they've been
assiged for the upcoming milestone. To weight issues, they should:

1. Look through the issues on the milestone with a `~"Deliverable"` label that have
   `Weight:None`.
2. For each issue, they add a weight. If possible, they also add a
   short comment explaining why they added that weight, what parts of the code
   they think this would involve, and any risks or edge cases we'll need to
   consider.
3. The process is intended to be lightweight. If something isn't clear what
   weight it is, they should ask for clarification on the scope of the issue.
4. Start adding weights around **a week before** the weights for a milestone
   are due. Finishing earlier is better than finishing later.
5. Check issues that have existing weights. Change the weight if you do not feel
   like the initial weight given is accurate and leave a comment explaining your reasoning.
6. When the issues for the next release get formally assigned, the assignee should
   investigate the issue and confirm the weight.

Adding weights to the issues should take no longer than 8 hours. Please speak up
if you find this taking longer than expected!

The rotation for upcoming releases is:

| Release | Weights due | Engineer        |
| ---     | ---         | ---             |
| 12.7    | 2019-12-17  | Patrick Bajao   |
| 12.8    | 2020-01-17  | ---             |
| 12.9    | 2020-02-17  | Igor Drozdov    |
| 13.0    | 2020-03-17  | Kerri Miller    |
| 13.1    | 2020-04-17  | Nick Thomas     |
| 13.2    | 2020-05-17  | Patrick Bajao   |
| 13.3    | 2020-06-17  | David Kim       |
| 13.4    | 2020-07-17  | Robert May      |
| 13.5    | 2020-08-17  | Gary Holtz      |


#### Backend and Frontend issues

Many issues require work on both the backend and frontend, but the weight of that work may not be the same. Since an issue can only have a single weight set on it, we use scoped labels instead when this is the case: `~backend-weight::<number>` and `~frontend-weight::<number>`.

### What to work on

<%= partial("handbook/engineering/development/dev/create/what_to_work_on.erb", locals: { group: "Source Code", slack_channel: 'g_create_source_code' }) %>

[issue board]: https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=group::source%20code&label_name[]=backend
[assignment board]: https://gitlab.com/groups/gitlab-org/-/boards/1143518

### Workflow labels

<%= partial("handbook/engineering/development/dev/create/workflow_labels.erb", locals: { group_label: 'group::source code' }) %>

### Async standup

<%= partial("handbook/engineering/development/dev/create/async_standup.erb") %>

### Retrospectives

We have 1 regularly scheduled "Per Milestone" retrospective, and can have ad-hoc "Per Project" retrospectives.

#### Per Milestone

<%= partial("handbook/engineering/development/dev/create/retrospectives.erb", locals: { group: "Source Code", group_slug: 'source-code' }) %>

#### Per Project

If a particular issue, feature, or other sort of project turns into a particularly useful learning experience, we may hold a synchronous or asynchronous retrospective to learn from it. If you feel like something you're working on deserves a retrospective:
1. [Create an issue](https://gitlab.com/gl-retrospectives/create-stage/source-code/issues) explaining why you want to have a retrospective and indicate whether this should be synchronous or asynchronous
2. Include your EM and anyone else who should be involved (PM, counterparts, etc)
3. Coordinate a synchronous meeting if applicable

All feedback from the retrospective should ultimately end up in the issue for reference purposes.

### Deep Dives

<%= partial("handbook/engineering/development/dev/create/deep_dives.erb") %>

### Career development

<%= partial("handbook/engineering/development/dev/create/career_development.erb", locals: { group: "Source Code" }) %>

### Performance Monitoring

The Create:Source Code BE team is responsible for keeping some API endpoints and
controller actions performant (e.g. below our target speed index).

Here are some Kibana visualizations that give a quick overview on how they perform:

- [Create::Source Code: Controller Actions](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/32698f60-b145-11ea-bfe2-25f984e253f8?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow)))
- [Create::Source Code: Endpoints](https://log.gprd.gitlab.net/app/kibana#/visualize/edit/104d4bf0-a0d9-11ea-8cfd-8dcd98a55a1d?_g=(filters%3A!()%2CrefreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-7d%2Cto%3Anow)))

These tables are filtered by the endpoints and controller actions that the group
handles and sorted by P90 (slowest first) for the last 7 days by default.
