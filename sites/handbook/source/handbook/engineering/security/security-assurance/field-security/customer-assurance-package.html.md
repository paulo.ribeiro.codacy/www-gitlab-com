---
layout: handbook-page-toc
title: "GitLab's Customer Assurance Package"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab's Customer Assurance Package

This handbook page is a collection of information that we have found to be helpful to our customers. While GitLab is transparent with as much information as possible, it can sometimes be hard to find specific information within the handbook so this page should be a useful way to access all the information your company would need to review the security program maturity of GitLab as a vendor.

This page will also be useful to the GitLab Sales team since providing this information early in the sales cycle can greatly reduce the time that a customer spends evaluating the security of GitLab and ensuring that integrating our product into their ecosystem does not pose undue risk  to their environment.

## CSA Consensus Assessments Initiative Questionnaire (CAIQ)
The [Cloud Security Alliance (CSA)](https://cloudsecurityalliance.org/about/) is 
a leader in Cloud Security and they have developed the 
[Consensus Assessments Initiative Questionnaire (CAIQ)](https://cloudsecurityalliance.org/artifacts/consensus-assessments-initiative-questionnaire-v3-1/) 
to help `cloud customers to gauge the security posture of prospective cloud service providers and determine if their cloud services are suitably secure`.

The CAIQ captures the vast majority of all questions normally found on information security questionnaires. GitLab customers can see our responses to these questions and all related handbook links and comments through [GitLab's CAIQ](https://cloudsecurityalliance.org/star/registry/gitlab/). This questionnaire is accessible to anyone and a Non-Disclosure Agreement (NDA) is **not required** to view this questionnaire.

## SOC2 Type 1 Report
GitLab has recently acquired its first SOC2 Type 1 report with the best possible outcome. This report validates that GitLab's security controls have been designed effectively. This is just one step in our security compliance journey but it should demonstrate to our customers that GitLab has made a strong committment to security and we have designed our security program in a way that provides strong security to our customers.

You can [**request a copy of the GitLab SOC2 type 1 report** through the instructions on this handbook page.](/handbook/engineering/security/security-assurance/security-compliance/soc2.html#requesting-a-copy-of-the-gitlab-soc2-type-1-report)

## GitLab Penetration Test Results
GitLab routinely undergoes a penetration test by an external firm, redacted results of
this test as well as remediation status may be provided to prospects and customers. 
For more information about penetration tests please see [our penetration testing policy](/handbook/engineering/security/penetration-testing-policy.html).

If you would like to request a copy of the penetration test, please follow the 
instructions in our [**external testing handbook page**](/security/#external-testing).

## Information About Security at GitLab
* The [GitLab Security Trust Center](/security/) is a page that outlines GitLab's stance on security as well as some 
answers and discussions to common customer questions.
* GitLab's Security Controls can be found on [this handbook page](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
* [Information Security Policies](/handbook/engineering/security/#information-security-policies)
    * [GitLab Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/)
    * [GitLab Password Policy](/handbook/security/#gitlab-password-policy-guidelines)
    * [GitLab Access Management Policy](/handbook/engineering/security/#access-management-process)
    * [GitLab Data Classification Policy](/handbook/engineering/security/data-classification-policy.html)
    * [GitLab Data Protection Impact Assessment Policy](/handbook/engineering/security/dpia-policy/)
    * [GitLab Penetration Testing Policy](/handbook/engineering/security/penetration-testing-policy)
    * [GitLab Audit Logging Policy](/handbook/engineering/security/audit-logging-policy.html)
* [GitLab Security Incident Response Guide](/handbook/engineering/security/sec-incident-response.html)
* [GitLab Business Continuity Plan](/handbook/business-ops/gitlab-business-continuity-plan.html)

## Information About Architecture of GitLab
* [GitLab Application Architecture](https://docs.gitlab.com/ee/development/architecture.html)
* [GitLab Production Architecture](/handbook/engineering/infrastructure/production/architecture/)
    * [High-level Network Diagram](/handbook/engineering/infrastructure/production/architecture/#network-architecture)

## Information About Securing Your GitLab Environment
* [GitLab instance: security best practices](/blog/2020/05/20/gitlab-instance-security-best-practices/)

## Questions?
* If you have any further questions that aren't answered here please:
    * If you are not a GitLab team-member, contact security@gitlab.com.
    * If you are a GitLab team-member, reach out to Field Security via slack [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70), or [open an issue](https://gitlab.com/gitlab-com/gl-security/field-security/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) on our issue board.
