---
layout: handbook-page-toc
title: "Core Jobs To Be Done (JTBD) principles"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Core Jobs To Be Done (JTBD) principles

**1. People employ products and services to get their job done, not to interact with your organization.**
* JTBD doesn’t look at the relationship that people have to a given solution or brand, but rather how a solution fits into their world. The aim is to understand their problems before coming up with solutions.
* JTBD is not about customer journeys or experiences with a product, which assume a relationship to a given provider. Customer journey investigations seek to answer questions such as: When do people first hear about a given solution? How did they decide to select the organization’s offerings? What keeps them using it? These are all important questions to answer, but they also don’t get to the underlying job.
* JTBD focuses on the relationships that people have with reaching their own objectives. A given solution may or may not be employed in the process, but the job exists nonetheless, independent of any one provider. 

**2. Jobs are stable over time, even as technology changes.**
*  References to solutions (products, services, methods, and so on) are carefully avoided in JTBD. Consequently, JTBD research is foundational and can be applied across projects and departments over time. There is an exception around disruptive technologies, which upon introduction to the market JTBDs can change. 


**3. People seek services that enable them to get more of their job to be done quicker and easier.**
* "Upgrade your user, not your product. Value is less about the stuff and more about the stuff the stuff enables. Don't build better cameras - build better photographers." - [**Kathy Sierra**](https://en.wikipedia.org/wiki/Kathy_Sierra)
 
**4. Making the job the unit of analysis makes innovation more predictable**

**5. JTBD are shared across disciplines and throughout organizations.**
*  JTBD detaches upfront understanding from implementation. It gives a consistent, systematic approach to understanding what motivates people, which allows various teams inside an organization to leverage JTBD:
   * Sales can leverage JTBD thinking in customer discovery calls to uncover the objectives and needs that prospects are trying to accomplish.
   * Marketing specialists can create more effective campaigns around JTBD by shifting language from features to needs. 
   * Customer success managers can use JTBD to understand why customers might cancel a subscription.
   * Support agents are able to provide better service by first understanding the customer’s job to be done. 
   * Business development and strategy teams can use insight from JTBD to spot market opportunities.