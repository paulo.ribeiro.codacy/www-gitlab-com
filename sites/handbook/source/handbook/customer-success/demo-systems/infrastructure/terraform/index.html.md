---
layout: handbook-page-toc
title: "Demo Systems Infrastructure - Terraform"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Overview

We use Terraform for managing all of our infrastructure. It is strongly recommended to read through the Terraform documentation to understand the syntax and organization of Terraform code. If you learn best from books, check out the [Terraform Up and Running 2nd Edition](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code/dp/1492046906).

See the README in the repository to learn more about our implementation and how to use Terraform.

### Repository

[https://gitlab.com/gitlab-com/customer-success/demo-systems/infrastructure/demosys-terraform](https://gitlab.com/gitlab-com/customer-success/demo-systems/infrastructure/demosys-terraform)
