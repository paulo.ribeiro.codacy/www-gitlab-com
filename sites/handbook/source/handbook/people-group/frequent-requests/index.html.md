---
layout: handbook-page-toc
title: "Frequently Requested"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Frequent requests that the team often receive may be answered below.  Please take a look before reaching out to the team.

## Team Directory
{: #directory}

GitLab uses Slack profiles as an internal team directory, where team members can add their personal contact details, such as email, phone numbers, or addresses. This is your one-stop directory for phone numbers and addresses (in case you want to send your teammate an awesome card!). Feel free to add your information to your Slack profile (this is completely opt-in!) by clicking on "GitLab" at the top left corner of Slack, "Profile & Account", then "Add Profile" (for the first time making changes) or "Edit Profile" (if your account is already set up) to make any changes!

- Please make sure that your address and phone information are written in such a way that your teammates can reach you from a different country. So, for example, include `+[country code]` in front of your phone number.

## Letter of Employment
{: #letter-of-employment}

If you need a letter from GitLab verifying your employment/contractor status, please fill out this [form](https://docs.google.com/forms/d/e/1FAIpQLSeop7IJJtN9OWasZl9S992sH5iNio0j916SJa0mBDgc2-2hNA/viewform). People Experience team will verify the accuracy and send you an email with the signed letter. We will also verify (but not provide) National Identification Numbers. In addition, if the request comes from a third party, People Experience team will always verify that the information is appropriate to share and check with you if you have given authorization to share any personal information.

## Letter Confirming Remote Employment
{: #letter-confirming-remote-employment}

If you need GitLab to create a letter confirming that you work remotely, please have the relevant company or organization request this by emailing `people-exp@gitlab.com`. You may also email the People Experience team directly if it is easier for you to provide the contact information of the organization requesting the letter. The People Experience team will complete this letter, sign it and email it to the company or organization who requested it.

## Employment Verification
{: #emplyoment-verification}

If you need GitLab People Ops to complete an employment verification form, usually for the purposes or a loan or security access, please have the relevant company or organization request this by emailing `people-exp@gitlab.com`, in the event that you are employed via a **PEO** (SafeGuard, Global Upside, CXC. Remote etc) you must request the information directly from them, if you require assistance on the contact details please email `peopleops@gitlab.com` . The People Experience team will complete the requested documents using HelloSign and request any information not in BambooHR from the relevant department, usually Payroll. The People Experience team will always verify that the information is appropriate to share and check with you to be sure that you have given authorization to share any personal information, if a signed consent form is not included in the request. The People Experience  team will sign the final form and send it to the requestor through an encrypted format.

## Request for GitLab's phone number
Team members often are asked for a company phone number to complete a mortgage application, loan request or similar application.  Our company phone number is outlined on the [GitLab Communication page](https://about.gitlab.com/handbook/communication/#phone-number) but this phone number will direct the caller to make the request via email.  If the requester requires speaking to a human, please email total-rewards@domain and someone on the Total Rewards team will provide their phone number based on availability.

## Reference Request
{: #reference-request}

You do not need permission from GitLab to give a personal reference, but GitLab team members are not authorized to speak on behalf of the company to complete reference requests for GitLab team members no longer working for GitLab. If a team member would like to give a personal reference based on their experience with the former team member, it must be preceded by a statement that the reference is not speaking on behalf of the company. To reinforce this fact, personal references should never be on company letterhead, and telephone references should never be on company time. Remember to always be truthful in reference checks, and instead of giving a majority negative reference, refuse to provide one. Negative references can result in legal action in some jurisdictions.

If an ex team member acted in a malicious way against GitLab we'll do a company wide announcement on the company call not to provide a reference.

## Paperwork (Werkgeversverklaring) people may need to obtain a mortgage in the Netherlands

When your employment contract is for a fixed period of time (e.g. 12 months) you'll need a "werkgeversverklaring".
This document describes your salary and states that your employer expects to continue to employ
you after the contract expires (assuming the performance of the team member doesn't degrade).
This document has to be filled in by hand, preferably using blue ink, and must be signed
and stamped. If there is no stamp (as is the case for GitLab) an extra letter (also signed)
must be supplied that states the employer has no stamp. While the language of these
documents doesn't matter, the use of Dutch is preferred.

Team members also have to provide a copy of a payslip that clearly states their
monthly salary and also their annual salary. These numbers must match the numbers on
the "werkgeversverklaring" down to the decimals. Mortgage providers may also require
you to provide information about your financial status, usually in the form of a report/screenshot
of your total financial status (including any savings you have). The requirements for
this seem to vary a bit between mortgage providers.

If you need a werkgeversverklaring, be sure to reach out to peopleops@domain. THe People Specialist team will then forward the request to our Payroll Vendor in the Netherlands, to ensure the document is created in Dutch.

## Business Cards
{: #business-cards}

Business cards can be ordered through Moo. Please let People Experience team know if you have not received access to your account on Moo by the end of your first week with GitLab. Place the order using your own payment and add it to your expense report. If you need any assistance, let a People Experience team know.

Once you are logged in, follow these steps to create your cards:

1. Select your currency in the upper right corner to ensure that your shipment is sent from the correct location.
1. Select the "+" sign in the upper right corner of your screen. (If you don't see the "+" sign then go to [this URL](https://www.moo.com/mbs/products/business-cards)).
1. Select "Business Cards".
1. Select your template (one has the Twitter & Tanuki symbol and cannot be removed, and one is free of those symbols).
1. Enter your information into the card.
1. Please remember to choose rounded corners.
1. Add the card to your cart and order! We ask for a default of 50 cards unless you are meeting with customers regularly.
1. Add the cards to your expense report under 'office supplies'.

### Business Cards - India

Since MOO ships business cards from outside India via DHL, DHL is mandatorily required to perform "Know Your Customer" (KYC) checks before delivery.
If you are a team member residing in India, please consider using the following tips while ordering business cards from MOO.

- Avoid filling in the "Company name" field when checking out your order to prevent the shipment being sent in the company's name instead of your name. It is only necessary to fill the "First Name" and "Last Name" fields.
- For verification, DHL matches the name and address on your proof of ID with the name and address in your consignment. So, when providing the address for delivery, make sure to provide the same address as on one of your proofs of ID.
- In a scenario where you do not have any ID associated with the address you intend to provide, consider shipping the item in the name of somebody who holds a valid proof of ID at that address, like a relative or a friend.
- Please check out the list of [valid KYC documents](https://dhlindia-kyc.com/forms/valid-kyc-docs.aspx#ind-indian) before placing the order.

### Business Cards-Special Characters
-If you are a team member who needs business cards with special characters, please reach out to the People Experience Team (people-exp@gitlab.com)and indicate how you would like the layout to look like.
-The People Experience Coordinator will inform the Moo team to make the template available on Moo. This takes 24-48 hours
-Once the template is available, the People Experience Coordinator will advice you and you can proceed and place your order.

## Name Change
{: #name-change}

To initiate a change in name please complete the following:

- Team member: Open an access request issue following our [handbook instructions](https://about.gitlab.com/handbook/business-ops/employee-enablement/it-ops-team/access-requests/#single-person-access-request) to request to change your name and email address in Google.
- Team member: Email peopleops@gitlab.com, total-rewards@gitlab.com, and payroll@gitlab.com linking your access request issue and including legal documents with proof of your requested name.
- People Ops: Update Name in BambooHR and GSuite.
