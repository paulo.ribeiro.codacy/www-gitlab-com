---
layout: markdown_page
title: "Combating burnout, isolation, and anxiety in the remote workplace"
canonical_path: "/company/culture/all-remote/mental-health/"
twitter_image: "/images/opengraph/all-remote.jpg"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how to recognize and avoid burnout in a remote setting. We'll also cover the issues of isolation and anxiety, and how to create a non-judgemental culture where teams are encouraged to work through it rather than internalize it.

## Do not celebrate working long hours

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/fH8nmtEoBh4?start=1579" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [People Group Conversation](https://youtu.be/fH8nmtEoBh4) above, GitLab CEO Sid Sijbrandij responds to a discussion on the topic of burnout and overwork.

> There's individual freedom, and there's peer pressure. As a company, we should take a lot of care that there's no peer pressure to work long hours.
>
> Everyone is used to that [being pressured]. At every company I've been at, that was a celebrated thing. We have to be super, super careful that we do not celebrate that at GitLab. — *Sid Sijbrandij, GitLab co-founder and CEO*

There's a fine line between [thanking someone publicly](/handbook/communication/#say-thanks) for going above and beyond to help out in a situation, and sending a message that work should always trump life.

Burnout rarely happens all at once. Rather, it typically takes one by surprise, eventually coming to a head after days, weeks, or months of overwork creep.

While working one additional hour to move a given project forward is likely not debilitating when viewed in a vacuum, it can trigger a revised baseline where you must *continue* to overwork in order to maintain the new status quo.

This becomes toxic when managers fail to recognize that a given sprint should not reset the baseline of what is achievable on an ongoing, sustained basis. It becomes disastrous when team members do not feel safe bringing this up to their managers in a [1:1 setting](/handbook/leadership/1-1/).

Particularly in a company where [results](/handbook/values/#results) are valued above all, managers should be careful to not assume that results garnered in a given period of overwork are the new norm. This places team members in an unfair scenario where they feel pressured to perpetually overwork in order to meet expectations. More broadly, as other team members witness this, they will be less likely to go above and beyond in special cases for fear of trapping themselves in a similar cycle of overworking just to meet ever-increasing (and unsustainable) expectations.

## Rest and time off are productive

Though it sounds counter to conventional wisdom, clarity comes through time away from work. Just as a human must inhale and exhale to survive, one cannot expect to remain healthy and productive if only inhaling more work. 

As John Fitch describes in the book [Time Off](https://www.timeoffbook.com/), there are four stages of creativity. This was initially [outlined](https://www.brainpickings.org/2013/08/28/the-art-of-thought-graham-wallas-stages/) by English social psychologist and London School of Economics co-founder Graham Wallas.

1. Preparation
1. Incubation
1. Illumination
1. Verification

[John Fitch](https://twitter.com/johnwfitch), co-author of [Time Off](https://www.timeoffbook.com/) and the Chief Product Officer at [Voltage Control](https://voltagecontrol.com/), articulates this on [The Culture Factor podcast](https://the-culture-factor.simplecast.com/episodes/what-if-time-off-created-a-culture-of-productivity-that-surpassed-12-hour-days). A portion of the interview is transcribed below.

> Incubation and illumination are only activated through time off — by not doing the actual work, by doing something else. While you're resting, parts of you are still working. I came to this contrarian belief that your best work is actually cultivated outside of work.
>
> 50% of the creative process requires you to *not* be working.  — *John Fitch*

Establishing a culture that gives voice to this reality is critical is removing the stigma from taking breaks and prioritizing wellbeing. Rest isn't at the expense of work; it's a core function of doing *excellent* work.

## Document processes around mental health

Burnout, isolation, and anxiety are issues that impact team members across all companies, regardless of their organizational structure. While they aren't always intertwined, there is significant interplay between them.

In a colocated setting, it's entirely possible for a team member to *appear* well, but struggle with these issues internally. That said, it tends to be easier for those in an office to reach out to a team member they trust (or their [people department](/handbook/people-group/)) if burnout, isolation, or anxiety is impacting their ability to thrive in the workplace.

In a remote setting, where [in-person interactions](/company/culture/all-remote/in-person/) are less common, it's easier to fall victim to [isolation](/company/culture/all-remote/drawbacks/). This is particularly true for those who are not well acclimated to remote work, or have [just started their first remote role](/company/culture/all-remote/getting-started/).

Because you are likely to work alone most times, it's more difficult to remember that you **do** have colleagues to call on — especially if you're already overwhelmed, burned out, or suffering from anxiety/depression.

### Create clarity through documentation

The aforementioned reality makes it all the more important for any company hiring remote workers to place a great deal of focus on [documenting processes](/handbook/total-rewards/benefits/modern-health/#what-does-modern-health-offer) for team members who face these difficulties. Along with offering professional assistance (see [GitLab Modern Health](/handbook/total-rewards/benefits/#employee-assistance-program) as an example), be sure to showcase documented resources of where to turn during [onboarding](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members), and reinforce this in ongoing [learning and development](/company/culture/all-remote/learning-and-development/) sessions. 

Remote team members may feel less comfortable reaching out to a person when experiencing mental duress, so it's vital to ensure that answers and resources are easily discoverable within a company's handbook.

### Leverage documentation to ease anxiety

This approach enables managers and leaders to experience less anxiety and worry of being a single point of failure. By documenting diligently, it is easier for managers to take time for themselves, prioritize family, and earnestly disconnect during holidays and vacation. 

[Documentation](/company/culture/all-remote/handbook-first-documentation/) allows a significant portion of one's managerial expertise to be tapped into even while the manager is away recharging, and this intentional decentralization creates a greater sense of calm for both leaders and direct reports. 

[John Fitch](https://twitter.com/johnwfitch), co-author of [Time Off](https://www.timeoffbook.com/) and the Chief Product Officer at [Voltage Control](https://voltagecontrol.com/), shares a powerful anecdote on this approach on [The Culture Factor podcast](https://the-culture-factor.simplecast.com/episodes/what-if-time-off-created-a-culture-of-productivity-that-surpassed-12-hour-days). A portion of the interview is transcribed below.

> My function of head of design and prototyping — if I'm the only one doing it, that's a problem. If I go away or something happens to me, that entire function is compromised. It's a single point of failure. When you decentralize, you get rid of single points of failure. 
>
> Clarity is a part of a decentralized organzation. That way, anyone can pick up and understand 'It's clear what we're here to do. Our values are clear.' 
>
> Much like an engineer will take their code and document it, that work is important. Another engineer can look at their code, and without emailing that person, make use of what has been created. Quality documentation has historically been a hallmark of engineering. We treat that the same regardless of function. When I'm away, I can be away. I don't have to worry about my team forgetting things.
>
> Just as journaling is important externally, it's powerful internally. Documenting my functions opens my eyes to self-improvement. As I'm writing things down, I can more clearly see better ways to do things. — *John Fitch*

## Creating a non-judgemental culture

[Transparency](/handbook/values/#transparency) is a core value at GitLab, and should be a value at any organization employing remote team members. Leaders should assume that some team members will feel uncomfortable surfacing issues involving isolation, burnout, and anxiety at work. This can stem from prior experiences, where bringing such issues to light could lead to negative consequences.

To combat this and destigmatize such issues, leadership should work to [build and sustain a non-judgemental culture](/company/culture/all-remote/building-culture/). This starts by celebrating a [diverse team](/handbook/values/#diversity--inclusion), and creating an [inclusive work environment](/company/culture/inclusion/).

At GitLab, we encourage team members to include overall feedback on how their life is going during [routine 1:1 meetings](/handbook/leadership/1-1/). Managers are responsible for creating a safe atmosphere, where team members can openly discuss issues related to mental health, and work with the team member to a resolution.

GitLab also offers a Slack channel — `#mental_health_aware` — dedicated to surfacing and discussing topics related to mental health.

### Force work into async tools

Working entirely or primarily in a chat tool such as Microsoft Teams or Slack is a pathway to burnout. Humans were not designed to have hundreds or thousands of people demanding things from them with red bubbles. There is a reason your phone can only allow one conversation at a time.

Leadership can create a more humane atmosphere by leaning on a tool (or tools) that enable asynchronous workflows, thereby [reducing meetings](/company/culture/all-remote/meetings/) and creating more time for focused, deep work. [GitLab uses GitLab](/solutions/gitlab-for-remote/) to accomplish this.

GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones. Originally, GitLab let software developers collaborate on writing code and packaging it up into software applications. Today, GitLab has a wide range of capabilities used by people around the globe in all kinds of companies and roles.

You can learn more at GitLab's [remote team solutions page](/solutions/gitlab-for-remote/).

## Workshop how to work less

There should be no stigma in questioning [efficiency](/handbook/values/#efficiency). What was ultimately most efficient a year ago may not be true today. If you sense that a team is overworking and creating a cycle of overwork for those in proximity, consider pausing to workshop how to work *less*. 

This environment creates a space where individuals can surface new tools and technologies which may be able to lighten the human load, or surface new realities in how the market has shifted to a point where certain elements of work are no longer as valuable. 

A regular cycle of these workshops creates moments for reevaluation. A rest and creativity ethic is just as important as a work ethic, particularly when you consider that *outstanding* work requires a certain amount of creativity and clarity. 

## How to recognize mental health struggles

Oftentimes, if you are feeling burned out, you aren't the only one feeling that way. GitLab team members have compiled a list of symptoms related to burnout, isolation, and anxiety [in a blog post](/blog/2018/03/08/preventing-burnout/). A few are highlighted below.

1. You're constantly tired

1. You no longer enjoy things

1. Your job performance suffers

1. Your physical health suffers (headaches, irregular breathing patterns, etc.)

1. Your relationships are strained

1. You feel socially zapped

1. You disable video for team calls to prevent others from seeing your pain

1. You are perpetually concerned with whether you are doing enough

1. You worry that your contributions are too few or too insignificant

1. You feel unable to [choose family first](/handbook/values/#family-and-friends-first-work-second)

## Working to prevent burnout, isolation, and anxiety

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Q9yjo6IOqX4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the video above, Darren (Head of Remote, GitLab) and Sara (Senior Partner Marketing Manager, GitLab) discuss tips, tricks, and insights on preventing burnout and achieving balance.*

Prevention is a [team sport](https://www.cncf.io/blog/2020/04/03/were-all-in-this-together-a-wellness-guide-from-the-cncf-well-being-working-group/). Leaders must work to establish a workplace culture that empowers rather than restricts, managers must be proactive in sensing the signs of mental strain, and team members must feel comfortable surfacing issues while they are still manageable. Below are several [recommendations](/blog/2018/03/08/preventing-burnout/) for avoiding and preventing burnout, according to GitLab team members.

1. Set clear boundaries between work and home
1. Take vacation
1. Take a "mental health" day to lower your stress (spend time outdoors, get a massage, get some exercise)
1. Know when to take a break
1. Put a [break reminder](https://apps.apple.com/us/app/time-out-break-reminders/id402592703) on your computer
1. Switch off when you're away from work
1. Don't suffer in silence
1. Don't go straight to work after you wake up
1. Remove Slack from your smartphone or at the very least, turn off notifications for it
1. Keep each other accountable. When you notice someone in a different time zone should be asleep, tell them
1. Use your Slack status to share a message with the team that you are unavailable
1. Schedule [random coffee breaks](/handbook/communication/#random-room)
1. You can use [informal communication](/company/culture/all-remote/informal-communication/) mechanisms such as virtual trivia sessions, talent shows, scavenger hunts, and global pizza parties/meals to help bond as a team and prevent work-related burnout.

### The power of being proactive

GitLab has added a number of changes to the company [handbook](/handbook/), encouraging managers and team members to be proactive when it comes to recognizing and avoiding burnout, isolation, and anxiety.

1. [Encourage team members to communicate with their manager when they recognize burnout](/handbook/paid-time-off/#recognizing-burnout)
1. [Encourage team members to notice signs of burnout in their peers and direct reports](/handbook/paid-time-off/#recognizing-burnout)
1. [Added tips to avoid burnout](/handbook/paid-time-off/#recognizing-burnout)

### Realistic expectations

Leadership must be sensible about expectations. If a company's [OKRs](/company/okrs/) (objectives and key results) and [KPIs](/handbook/ceo/kpis/) (key performance indicators) are unattainable without compromising company [values](/company/culture/all-remote/values/), this incongruence is a recipe for fostering burnout, isolation, and anxiety across a team.

It is foolish to expect a team member to maintain excellent mental health when their workload requires a sustained amount of sacrifice. There is a fine line between collaborating with a team member on an ambitious goal and assigning a task that will be perceived as impossible.

This nuance requires a leader who is adept at understanding a team member's strengths and weaknesses. What is perceived as impossible for one team member may seem trivial to another; it is not always the task that triggers duress, but mismatching a task with an ill-equipped team member.

This can be more pronounced in a remote setting. Leaders should pay close attention to blockers and struggles, and be proactive in asking about these during [1:1 sessions](/handbook/leadership/1-1/suggested-agenda-format/). Phrasing questions such as "Are there any assignments that you do not feel comfortable or equipped to handle?" is a better way to uncover truth compared to a blanket "Why isn't this working?"

It's also important to understand that not every team members prefers to discuss these topics using the same medium. While some may prefer video communication, others may prefer voice, writing, or something else. Remote leaders should strive to be [inclusive](/handbook/values/#diversity--inclusion) when searching for answers and solutions.

### Sentiment tracking and feedback

Particularly in remote companies, leadership should consider implementing processes around internal feedback. Companies will often wait to gather [internal feedback](/company/culture/internal-feedback/) until an exit interview after someone's resigned, or they'll organize an occasional survey to take a pulse on the company’s engagement. GitLab prefers shorter, but more frequent, check-ins, aligned to our values of [collaboration](/handbook/values/#collaboration) and [iteration](/handbook/values/#iteration).

Ask [questions](/blog/2019/05/16/building-an-award-winning-culture-at-gitlab/) that will shed light on whether or not a team member is thriving or struggling, and pay close attention to any adjustable workplace factors that are [contributing](/blog/2018/06/26/iterating-improving-frontend-culture/) either positively or negatively.

Learn more about [GitLab's approach and guidance on feedback](/handbook/people-group/guidance-on-feedback/).

## GitLab Knowledge Assessment: Combating burnout, isolation, and anxiety in the remote workplace

Anyone can test their knowledge on considerations for a productive home office or remote workspace by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSfMdzohD0mO_Ce7dBPrzFIKtJzROiN7aIxnJRvTu34akr-iEg/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been completed, you will receive an email acknowledging the completion from GitLab with a summary of your answers. If you complete all knowledge assessments in the [Remote Work Foundation](/company/culture/all-remote/remote-certification/), you will receive an unaccredited [certification](/handbook/people-group/learning-and-development/certifications/). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

## Is this advice any good?

![GitLab all-remote team illustration](/images/all-remote/gitlab-com-all-remote-1280x270.png){: .shadow.medium.center}

GitLab is the world's largest all-remote company. We are 100% remote, with no company-owned offices *anywhere* on the planet. We have over 1,300 team members in more than 65 countries. The primary contributor to this article ([Darren Murph](/handbook/marketing/readmes/dmurph/), GitLab's Head of Remote) has over 14 years of experience working in and reporting on colocated companies, [hybrid-remote](/company/culture/all-remote/hybrid-remote/) companies, and all-remote companies of various scale. 

Just as it is valid to [ask if GitLab's product is any good](/is-it-any-good/), we want to be transparent about our expertise in the field of remote work. 

## Contribute your lessons

Creating a healthy remote workplace is essential to business success. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
