---
layout: job_family_page
title: "Security Analyst"
---
## Job Grade
The roles described below are grades: [6, 7, 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

## Responsibilities
As a member of the [Security Team](/handbook/engineering/security) at GitLab, you will be working towards raising the bar on security for GitLab, Inc. the company, GitLab the product, and GitLab.com. We achieve that by collaborating with cross-functional teams to provide guidance on security best practices accross the organization, implementing security requirements and improvements, and reacting to security events and incidents.

## Requirements 
- Ability to use GitLab
- You share our [values](/handbook/values), and work in accordance with those values
- You have a passion for security and open source

## Levels

### Analyst 

#### Security Analyst Responsibilities
- As defined by specialty.

#### Security Analyst Requirements
- You are a team player, and enjoy collaborating with cross-functional teams
- You are a great communicator
- You employ a flexible and constructive approach when solving problems

### Senior Analyst 

#### Senior Security Analyst Responsibilities

- As defined by specialty.

#### Senior Security Analyst Requirements

- All requirements for a Security Analyst; plus: 
- You have strong critical thinking and problem solving skills
- You have the ability to build simple solutions to complex problems
- You assist with recruiting activities and employee onboarding training 
- You prioritize collaboration across teams outside of Security
- You have the ability to iterate and improve on existing processes and programs
- You have the ability to build consensus without formal authority
- You have the ability to operate effectively in ambiguity
- You have strong knowledge in most GitLab tools, services, and infrastructure

### Staff Analyst

#### Staff Security Analyst Responsibilities
- As defined by specialty.

#### Staff Security Analyst Requirements

- All requirements for a Senior Security Analyst; plus: 
- You are an industry recognized leader in your field
- You have a proven ability to create new security programs and deliver successful results
- You execute program-level leadership across teams inside and outside of Security
- You have a detailed and comprehensive knowledge of all GitLab tools, services, and infrastructure

## Specialties
### Anti-Abuse
The Senior Anti-Abuse Analyst is responsible for leading and implementing the various initiatives that relate to improving GitLab's security.

#### Senior Anti-Abuse Security Analyst Responsibilities

- Handle tickets/requests escalated to abuse
- Handle DMCA, phishing, malware, botnet, intrusion attempts, DoS, port scanning, spam, spam website, PII and web-crawling abuse reports to point of mitigation of abuse
- Verify proper classification of incoming abuse reports
- Execute messaging to customers on best practices
- Monitoring email, forums, and other communication channels for abuse, and responding accordingly
- Assist with recruiting activities and administrative work
- Making sure internal knowledge reference pages are updated
- Handle communications with independent vulnerability researchers and triage reported abuse cases.
- Educate other developers on anti-abuse cases, workflows and processes.
- Ability to professionally handle communications with outside researchers, users, and customers.
- Ability to communicate clearly on anti-abuse issues.

### Security Compliance

Security Compliance Analysts focus on defining and shaping GitLab’s security compliance programs and are experts in all things security compliance. We are looking for people who are comfortable building transparent compliance programs and who understand how compliance works with cloud-native technology stacks.

#### Security Compliance Analyst Responsibilities

- Conduct security control tests, identify observations and make recommendations
- Conduct vendor security reviews, identify observations and make recommendations
- Conduct security risk assessments and advise on treatment plans
- Support internal and external auditors or advisors as needed
- Professionally handle communications with internal and external stakeholders
- Communicate clearly on compliance issues
- Educate control owners on compliance workflows and processes
- Maintain handbook documentation and execute other administrative tasks

#### Security Compliance Analyst Additional Requirements
- Proven experience with security compliance programs
- Demonstrated experience with at least two security control frameworks (e.g. SOC 2, ISO, NIST, COSO, COBIT, etc.)
- Passion for transparent compliance programs
- Working understanding of how compliance works with cloud-native technology stacks

#### Senior Security Compliance Analyst Responsibilities
- All responsibilities for a Security Compliance Analyst; plus:
- Participate in security assurance roadmap development based on customer needs
- Execute end to end compliance initiatives in accordance with the compliance roadmap
- Lead external audits and coordinate audit evidence
- Build and maintain security controls that map to GitLab security compliance requirements
- Design high quality test plans to execute against security compliance controls
- Operate existing controls and collect audit evidence with minimal guidance
- Define compliance requirements that do not adversely impact GitLab teams yet still solve underlying security compliance needs and provide recommendations on those requirements to other team members
- Educate control owners on complex compliance requirements 

#### Senior Security Compliance Analyst Additional Requirements
- All requirements for a Security Analyst; plus:
- Proven experience defining and shaping compliance programs
- Demonstrated experience with at least four security control frameworks (e.g. SOC 2, ISO, NIST, COSO, COBIT, etc.)

#### Staff Security Compliance Analyst Responsibilities
- The number of Staff Security Compliance Analysts are very limited and it is unlikely that GitLab would ever have more than one at a time given the level of expertise involved with achieving this level in this field.
- All responsibilities for a Senior Security Security Analyst; plus:
- Anticipate external audit requirements in advance
- Predict future industry trends to keep GitLab on the bleeding edge of Security Compliance
- Create dynamic open-source security compliance programs that deliver value to the GitLab community
- Mentor other Security Compliance Analysts and improve quality and quantity of the team's output
- Regularly present at conferences
- Build the GitLab Security Compliance brand

#### Staff Security Compliance Analyst Additional Requirements
- All requirements for a Senior Security Analyst; plus:
- Proven experience building compliance programs from the ground-up
- Proven experience with successful first-time external audits
- Expertise with at least six security control frameworks (e.g. SOC 2, ISO, NIST, COSO, COBIT, etc.)

### Field Security 
Field Security analysts serve as the public representation of GitLab's Security Department. Their primary goal is to provide high levels of assurance to GitLab customers and prospects regarding GitLab's security posture and to help drive maturation of the Security program.

#### Field Security Analyst Responsibilities

- Manage, coordinate, and resolve all external security requests for the GitLab Security Department within SLA
- Support the pre-sales and customer support processes including:
    * Fielding questions about GitLab's security program and features from existing and potential customers
    * Participate in calls discussing GitLab's security posture
    * Completing security assessments and questionnaires within SLA
    * Maintain the RFP
    * Maintain the Customer Assurance Package
    * Maintain the Trust site
- Maintain security training and collateral to support Sales Enablement
- Maintain handbook documentation and execute other administrative tasks
- Advise internal teams on security requests from the field

#### Field Security Analyst Additional Requirements
- Proven experience with security compliance programs
- Proven experience with sales enablement of customer deals exceeding $50,000 in annual revenue
- Ability to communicate clearly on related security issues with non security professionals

#### Senior Field Security Analyst Responsibilities

- All responsibilities for a Security Analyst; plus:
- Lead and mature security training sales enablement programs
- Participate in security assurance roadmap development based on customer needs
- Align other security analyst activities with the field security roadmap
- Continuously develop and mature security documentation that assures prospects and customers about the state of GitLab's security program reducing the need for security questionnaire responses
- Publish regular blog posts informing internal and external stakeholders about changes to GitLab's security program

#### Senior Field Security Analyst Additional Requirements

- All requirements for a Security Analyst; plus:
- Proven experience with sales enablement of customer deals exceeding $250,000 in annual revenue

#### Staff Field Security Analyst Responsibilities

- All responsibilities for a Senior Field Security Analyst; plus:
- Establish strong relationships with Security and Sales leaders inside and outside of GitLab
- Predict future industry trends to keep GitLab on the bleeding edge of Security initiatives and customer demands 
- Create open source field security programs that deliver high value to the GitLab Community
- Mentor other Field Security Analysts and improve quality and quantity of the team's output
- Regularly publish industry recognized collateral around best in class security programs and initiatives
- Regularly present at conferences
- Build the GitLab Field Security and Security brands

#### Staff Field Security Analyst Additional Requirements
- All requirements for a Senior Field Security Analyst; plus:
- Proven expert in complex Security Compliance initiatives and programs 
- Proven experience leading complex Field Security projects from the ground-up
- Proven experience with sales enablement of customer deals exceeding $1,000,000 in annual revenue

### External Communications
The External Communications Team leads customer advocacy, engagement and communications in support of GitLab Security Team programs. Initiatives for this specialty include:

#### Senior External Communications Analyst Responsibilities

- Increase engagement with the hacker community, including our public bug bounty program.
- Build and manage a Security blogging program.
- Develop social media content and campaigns, in collaboration with GitLab social media manager.
- Manage security alert email notifications.
- Collaborate with corporate marketing, PR, Community Advocates and Technical Evangelism teams to help identify opportunities for the Security Team to increase industry recognition and thought leadership position.

### Security Operations

[Security Operations](/handbook/engineering/security/#security-operations) is responsible for the proactive security measures to protect GitLab the company, GitLab the product, and GitLab.com, as well as detecting and responding to security incidents.  The Security Analysts in Security Operations play a vital role in identifying and responding to incidents, and using the resulting knowledge and experience to help build automated methods of remediating these issues in the future.

#### Security Operations Analyst Responsibilities

- Respond and assist with security requests and incidents submitted by GitLab team-members
- Review logging, alerting, and audit sources to identify potential security incidents
- Act on security incidents identified through monitoring and alerting sources
- Contribute to the creation and upkeep of runbooks to handle security incidents
- Work closely with the Security Operations Engineers to improve incident alertings and automated remediation

#### Senior Security Operations Analyst Responsibilities

- In addition to the responsibilities of a Security Analyst in Security Operations:
- Leverages security expertise in at least one specialty area
- Triage and act on escalated security incidents independently
- Conduct incident RCA's and propose security improvements to prevent or minimize future incidents
- Screen security candidates during the hiring process
- Mentor Security Analyst to improve technical and procedural skills

## Performance Indicators

Security Analysts have job-family performance indicators defined by each sub department leader. 

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule an interview with the Hiring Manager
- Candidates will then be invited to schedule an interview with the Hiring Manager defined panel
- Candidates will then be invited to schedule an additional interview with the sub-department Director
- Finally, candidates may be asked to interview with the VP of Security, EVP of Engineering or CEO (at leadership discretion)
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Compa Groups

General definitions for compa groups can be found on the [GitLab Global Compensation handbook page](/handbook/total-rewards/compensation/compensation-calculator/#compa-ratio). Below is a more targeted definition of what it means to be in a compa group specific to security analyst roles.

- Learning: New to this level with regular input and guidance needed to accomplish responsibilities.
- Growing:  The ability to operate independently on the vast majority of day-to-day tasks with more guidance needed on new programs and initiatives.
- Thriving: The ability to operate independently on the programmatic level with strong cross-team communication. This is the compa group that most security analysts will spend the most time in within a particular role. This compa group involves regularly operating outside of your comfort zone, continually seeking out new work, and helping to distribute work across the team to ensure team-level OKR's are met. Individual OKR expectations are regularly met.
- Expert: Guidance on tasks and projects is rarely needed and there is very little left to learn within the responsibilities and requirements of this role. All work is performed transparently and with a sense of urgency. OKR expectations are regularly exceeded and data exists to support expertise in all areas of responsibilities and requirements.

## External Communications
* [HackerOne Outreach and Engagement](/handbook/engineering/security/performance-indicators/#hackerone-outreach-and-engagement)