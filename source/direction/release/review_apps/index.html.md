---
layout: markdown_page
title: "Category Direction - Review Apps"
---

- TOC
{:toc}

## Review Apps

Review Apps let you build a review process right into your software development workflow
by automatically provisioning test environments for your code, integrated right
into your merge requests.

This ties into our [progressive delivery](https://about.gitlab.com/direction/ops/#progressive-delivery) vision of CI/CD as it gives you a glipse of how  your application will look after a specific commit, way before it reaches production.
Our ultimate goal is that Review Apps should spin up with a one-click button that works automatically regardless of the deployment target (this includes cloud-native and mobile as well). Anyone can view, comment and even fix any errors found directly from the review app itself.

![Review Apps]( /images/direction/cicd/review-apps.png) 

This area of the product is in need of continued refinement to add more kinds of
review apps (such as for mobile devices), and a smoother, easier to use experience.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AReview%20Apps)
- [Overall Vision](/direction/ops/#release)
- [Documentation](https://docs.gitlab.com/ee/ci/review_apps/)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3AReview%20Apps) 
- [Research insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3AReview%20Apps)


### Visual Reviews

We  added the ability to comment directly on the review apps [gitlab#10761](https://gitlab.com/gitlab-org/gitlab/issues/10761), With a small code snippet, users can enable designers, product managers, and other stakeholders to quickly provide feedback on a merge request without leaving the app.

## What's Next & Why

We are currently working on bug fixes related to Review Apps. If there is an issue that you are interested in, please let us know by providing feedback in the issue itself our by contributing it and we will make sure to review it.

## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/#maturity-plan)).
Key deliverables to achieve this are:
- [Extend the API to provide an ability to run scripts to delete environment](https://gitlab.com/gitlab-org/gitlab/issues/5582) (Complete)
- [Optional expiration time for environments](https://gitlab.com/gitlab-org/gitlab/issues/20956) (Complete)
- [Truly dynamic environment URLs](https://gitlab.com/gitlab-org/gitlab/issues/17066) (Complete)
- [One button to enable review apps, auto-edit `.gitlab-ci.yml`, auto-configure GKE](https://gitlab.com/groups/gitlab-org/-/epics/2349) (Partial)
- [In-browser Review Apps for iOS/Android development](https://gitlab.com/gitlab-org/gitlab/issues/20295) (Complete)
- [Mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/issues/19724)
- [First-class review apps](https://gitlab.com/gitlab-org/gitlab/issues/18982)

## Competitive Landscape

One big advantage Heroku Review Apps have over ours is that they are easier to set up
and get running. Ours require a bit more knowledge and reading of documentation to make
this clear. We can make our Review Apps much easier (and thereby much more visible) by
implementing [One button to enable review apps, auto-edit `.gitlab-ci.yml`, auto-configure GKE](https://gitlab.com/groups/gitlab-org/-/epics/2349),
which does the heavy lifting of getting them working for you.

## Top Customer Success/Sales Issue(s)

Management of Review Apps can be a challenge, particularly in cleaning them up. To
highlight the severity of how this issue can grow, www-gitlab-com project has over
1,500 running stale environments at the time of writing this, with no clear easy way
to clean them up. There are two main items that look to address this challenge:

- [gitlab#20956](https://gitlab.com/gitlab-org/gitlab/issues/20956) builds in an expiration date for review apps, beyond which they will automatically be terminated (Delivered in 12.8).
- [gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724) (also the 2nd customer issue) implements a way to clean up environments that either did not have an expiration date or were not terminated for other reasons.

Review Apps for GitLab Pages is a highly requested functionality [gitlab#16907](https://gitlab.com/gitlab-org/gitlab/issues/16907).

## Top Customer Issue(s)

[gitlab#13249](https://gitlab.com/gitlab-org/gitlab/-/issues/13249) allows review apps to leverage downstream deployment projects. This is especially useful when using a micro services architecture, where individual microservices have to rely on others to execute successfully.  Review apps automatically stops when a merge request is merged and the current behavior is not smooth across multi-repos (For example: Frontend, Backend, Combo Repo).

## Top Internal Customer Issue(s)

In some cases after a review app is not available for a specific Merge Request  
([gitlab#25351](https://gitlab.com/gitlab-org/gitlab/issues/25351)), adding some 
information to the user as to what action needs to be done in order to resolve this and
taker action directly from the MR itself, would help not only our customers, 
but also our own developers. 

## Top Vision Item(s)

Our focus for the vision is to bring Review Apps to mobile workflows via
[gitlab#2372](https://gitlab.com/groups/gitlab-org/-/epics/2372) - adding
support to Android/iOS emulators via the Review App will enable a whole new kind
of development workflow in our product, and make Review Apps even more valuable. 




